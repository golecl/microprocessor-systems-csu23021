#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

float calculatePiFloat(int iterations){
    uint64_t start = time_us_64();
    float pi = 1;
    float one, two, fraction1, fraction2;
    one = 1;
    two = 2;
    for(int i = 1; i <= iterations; i++){
        fraction1 = (two * i)/((two * i) - one); // working out the first fraction
        fraction2 = (two * i)/((two * i) + one); // working out the second fraction
        pi = pi * (fraction1 * fraction2);
    }
    uint64_t end = time_us_64();
    double total = end - start;
    printf("Single precision execution time = %.0f\n", total);
    return 2 * pi;
}

double calculatePiDouble(int iterations){
    uint64_t start = time_us_64();
    double pi = 1;
    double one, two, fraction1, fraction2;
    one = 1;
    two = 2;
    for(int i = 1; i <= iterations; i++){
        fraction1 = (two * i)/((two * i) - one); // working out the first fraction
        fraction2 = (two * i)/((two * i) + one); // working out the second fraction
        pi = pi * (fraction1 * fraction2);
    }
    uint64_t end = time_us_64();
    double total = end - start;
    printf("Double precision execution time = %.0f\n", total);
    return 2 * pi;
}

// Main code entry point for core0.
int main() {

    const int    ITER_MAX   = 100000;
    stdio_init_all();
    multicore_launch_core1(core1_entry);
    uint64_t singleTotalTimeStart, singleTotalTimeEnd;
    double singleTotalTime, parallelTotalTime;
    uint64_t parallelTotalTimeStart, parallelTotalTimeEnd;

    singleTotalTimeStart = time_us_64();
    calculatePiFloat(ITER_MAX);
    calculatePiDouble(ITER_MAX);
    singleTotalTimeEnd = time_us_64();
    singleTotalTime = (double) (singleTotalTimeEnd - singleTotalTimeStart);
    printf("Total execution time for both functions with single core = %.0f\n\n", singleTotalTime);

    parallelTotalTimeStart = time_us_64();
    multicore_fifo_push_blocking((uintptr_t) calculatePiDouble);
    multicore_fifo_push_blocking(ITER_MAX);
    calculatePiFloat(ITER_MAX);
    multicore_fifo_pop_blocking();
    parallelTotalTimeEnd = time_us_64();
    parallelTotalTime = (double) parallelTotalTimeEnd - parallelTotalTimeStart;
    printf("Total execution time for both functions with multi core = %.0f\n\n\n", parallelTotalTime);

    // Code for sequential run goes here…
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation
    //    Run the double-precision Wallis approximation
    //    Take snapshot of timer and store
    //    Display time taken for application to run in sequential mode

    // Code for parallel run goes here…
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation on one core
    //    Run the double-precision Wallis approximation on the other core
    //    Take snapshot of timer and store
    //    Display time taken for application to run in parallel mode

    return 0;
}