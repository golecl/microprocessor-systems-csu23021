#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.

float calculatePiFloat();
double calculatePiDouble();

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
int main() {
    // initating all of the variables as the appropriate class types
    float piFloat;
    double piDouble;
    double piControlDouble = 3.14159265359;
    float piControlFloat = 3.14159265359;
    float approxFloatError;  
    double approxDoubleError;    
    piFloat = calculatePiFloat(); // calculating the pi Willis approximation as a float
    piDouble = calculatePiDouble(); // calculating the pi Willis approximation as a float

    approxFloatError = piControlFloat - piFloat; // calculating the approximation error of the pi float
    approxDoubleError = piControlDouble - piDouble; // calculating the approximation error of the pi double

    printf("The float Willis approximation pf pi is %.11f\n", piFloat);
    printf("The double Willis approximation of pi is %.11f\n", piDouble);
    printf("The approximation error of the float is %.11f\n", approxFloatError);
    printf("The approximation error of the double is %.11f\n", approxDoubleError);
    // Returning zero indicates everything went okay.
    return 0;
}

float calculatePiFloat(){
    float pi = 1;
    float one, two, fraction1, fraction2;
    one = 1;
    two = 2;
    for(int i = 1; i <= 100000; i++){
        fraction1 = (two * i)/((two * i) - one); // working out the first fraction
        fraction2 = (two * i)/((two * i) + one); // working out the second fraction
        pi = pi * (fraction1 * fraction2);
    }
    return 2 * pi;
}

double calculatePiDouble(){
    double pi = 1;
    double one, two, fraction1, fraction2;
    one = 1;
    two = 2;
    for(int i = 1; i <= 100000; i++){
        fraction1 = (two * i)/((two * i) - one); // working out the first fraction
        fraction2 = (two * i)/((two * i) + one); // working out the second fraction
        pi = pi * (fraction1 * fraction2);
    }
    return 2 * pi;
}
